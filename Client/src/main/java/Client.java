import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static final int PORT = 6543;
    private Socket socket = null;
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    private Scanner scanner;

    public void start() throws Exception {
        scanner = new Scanner(System.in);

        boolean conn = connect();

        if (!conn) return;

        /*We are connected to server.*/
        requestLoop();
    }

    private boolean connect() throws IOException, ClassNotFoundException {
        System.out.println("Do you have an account?[y/n]");
        String response = scanner.next();
        while (!response.equals("y") && !response.equals("n")) {
            System.out.println("Please, type y or n.");
            response = scanner.next();
        }

        while (true) {
            socket = new Socket("localhost", PORT);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());

            if (response.equals("y")) {
                return connectUser();
            } else {
                addUserAndConnect();
                return true;
            }

        }
    }

    private boolean connectUser() throws IOException, ClassNotFoundException {
        System.out.println("Please, write your username: ");
        String username = scanner.next();
        String message = "conn" + username;
        outputStream.writeObject(message);

        String receivedPacket = (String) inputStream.readObject();
        System.out.println(receivedPacket);
        if (receivedPacket.equals("This username does not exists. Please try again!") ||
                receivedPacket.equals("You are already logged in.")) {
            socket.close();
            return false;
        }

        return true;
    }

    private void addUserAndConnect() throws IOException, ClassNotFoundException {

        boolean userAdded = false;
        while (!userAdded) {
            System.out.println("Please, write a username: ");
            String username = scanner.next();
            String message = "create" + username;
            outputStream.writeObject(message);
            /*Now, we add a new user. If name is already existing we are retrying to add a new user.*/
            String receivedPacket = (String) inputStream.readObject();
            if (receivedPacket.equals("You are registered and connected now.")) {
                userAdded = true;
            }
            System.out.println(receivedPacket);
        }
    }

    private void requestLoop() throws IOException, ClassNotFoundException {
        String menu = "Please, choose an option:\n" +
                "1. Show me topic list.\n" +
                "2. Subscribe to a topic.\n" +
                "3. Show my topics.\n" +
                "4. Find news about a topic.\n" +
                "0. Logoff\n";

        boolean isClose = false;
        while (!isClose) {
            System.out.println(menu);
            System.out.println("Choose: ");
            String option = scanner.next();

            /*We send a package to server.*/
            String requestMessage = requestMessage(option);
            if (requestMessage != null) {
                outputStream.writeObject(requestMessage);

                /*We are getting the response from server.*/
                String responseMessage = (String) inputStream.readObject();
                System.out.println(responseMessage);
            }

            if (option.equals("0")) {
                isClose = true;
            }
        }
        outputStream.close();
        inputStream.close();
        socket.close();
    }

    private String requestMessage(String option) {
        /*Here we are generating messages for server.*/
        if (option.equals("1")) {
            return "1";
        }
        if (option.equals("2")) {
            System.out.println("Please, type the topic you want to subscribe to.");
            String topicName = scanner.next();
            return "2" + topicName;
        }
        if (option.equals("3")) {
            return "3";
        }
        if (option.equals("4")) {
            System.out.println("Please, type the topic.");
            String topic = scanner.next();
            return "4" + topic;
        }
        if (option.equals("0")) {
            return "0";
        }

        return null;
    }
}
