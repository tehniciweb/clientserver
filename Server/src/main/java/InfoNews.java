public class InfoNews {
    private final String info;
    private final Topic topic;

    public InfoNews(Topic topic, String info){
        this.topic =topic;
        this.info = info;
    }

    @Override
    public String toString() {
        return info + "\n";
    }
}
