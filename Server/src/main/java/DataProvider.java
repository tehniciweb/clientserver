import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataProvider {

    private List<Topic> topics;
    /*name, interest topics*/
    private Map<String, List<Topic>> clients;
    private Map<Topic, List<InfoNews>> news;
    private final List<String> loggedClients;

    public DataProvider() {
        loggedClients = new ArrayList<>();
        generateTopicsAndNews();
        generateClients();
    }

    private void generateTopicsAndNews() {
        /*Generate multiple topics and news.*/
        topics = new ArrayList<>();
        news = new HashMap<>();

        topics.add(new Topic("sport"));
        List<InfoNews> newsSport = new ArrayList<>();
        newsSport.add(new InfoNews(topics.get(0), "England are set to host a bumper international summer in 2021 – followed by a similarly packed winter that includes a first trip to Pakistan in 16 years – under proposed schedules released on Wednesday."));
        newsSport.add(new InfoNews(topics.get(0), "Playing international football for Andorra is a constant battle against the odds, but one that Ildefons Lima has always embraced."));
        news.put(topics.get(0), newsSport);

        topics.add(new Topic("music"));
        List<InfoNews> newsMusic = new ArrayList<>();
        newsMusic.add(new InfoNews(topics.get(1), "Ariana Grande returns with new single ‘Positions’"));
        newsMusic.add(new InfoNews(topics.get(1), "Selena Gomez is singing in Africa."));
        newsMusic.add(new InfoNews(topics.get(1), "Fuego is sad. Christmas concert is canceled."));
        news.put(topics.get(1), newsMusic);

        topics.add(new Topic("nutrition"));
        List<InfoNews> newsNutrition = new ArrayList<>();
        newsNutrition.add(new InfoNews(topics.get(2), "Tomatoes are fruits."));
        newsNutrition.add(new InfoNews(topics.get(2), "10 tricks to burn calories"));
        news.put(topics.get(2), newsNutrition);

        topics.add(new Topic("movies"));
        List<InfoNews> newsMovies = new ArrayList<>();
        newsMovies.add(new InfoNews(topics.get(3), "In January will start a new season of `La Casa del Papel`."));
        news.put(topics.get(3), newsMovies);
    }

    private void generateClients() {
        /*Username of a client is unique. Username is used to identify a client.
         In this method we are associating for each client some topics.*/
        clients = new HashMap<>();
        ArrayList<Topic> biancaTopics = new ArrayList<>();
        biancaTopics.add(getTopicByName("movies"));
        clients.put("Bianca", biancaTopics);

        ArrayList<Topic> raduTopics = new ArrayList<>();
        raduTopics.add(getTopicByName("sport"));
        raduTopics.add(getTopicByName("nutrition"));
        raduTopics.add(getTopicByName("movies"));
        clients.put("Radu", raduTopics);

        ArrayList<Topic> mariaTopics = new ArrayList<>();
        mariaTopics.add(getTopicByName("nutrition"));
        mariaTopics.add(getTopicByName("movies"));
        clients.put("Maria", mariaTopics);
    }

    public String getAllTopics() {
        StringBuilder topicsName = new StringBuilder("List of topics: ");
        for (Topic topic : topics)
            topicsName.append(topic.getName()).append(" ");

        return topicsName.toString();
    }

    public Topic getTopicByName(String name) {
        for (Topic topic : topics)
            if (topic.getName().equals(name))
                return topic;

        return null;
    }

    public boolean containsTopic(String name) {
        for (Topic topic : topics)
            if (topic.getName().equals(name))
                return true;

        return false;
    }

    public String getClientTopics(String client) {
        StringBuilder topics = new StringBuilder("Your topics are: ");
        List<Topic> clientTopics = clients.get(client);

        for (Topic clientTopic : clientTopics)
            topics.append(clientTopic.getName()).append(" ");
        topics.append(".");

        return topics.toString();
    }

    public boolean clientSubscribeTopic(String client, String topic) {
        List<Topic> clientTopics = clients.get(client);

        for (Topic clientTopic : clientTopics)
            if (clientTopic.getName().equals(topic))
                return true;

        return false;
    }

    private Topic findTopic(String topic){
        for (Topic top:topics) {
            if(top.getName().equals(topic))
                return top;
        }
        return null;
    }

    public String getNewsAboutATopic(String client, String topic){
        StringBuilder newsStr = new StringBuilder();

        if(containsTopic(topic)){
            if(clientSubscribeTopic(client, topic)){
                List<InfoNews> newsList = news.get(findTopic(topic));
                return newsList.toString();
            }else{
                newsStr.append("You are not subscribing this topic.");
            }
        }else{
            newsStr.append("This topic does not exists.");
        }

        return newsStr.toString();
    }

    public Map<String, List<Topic>> getClients() {
        return clients;
    }

    public List<String> getLoggedClients() {
        return loggedClients;
    }
}
