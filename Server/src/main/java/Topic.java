public class Topic {

    private final String name;

    public String getName() {
        return name;
    }

    public Topic(String name) {
        this.name = name;
    }
}
