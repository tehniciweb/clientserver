import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

public class ServerThread extends Thread {

    private final DataProvider dataProvider;
    private final Socket socket;
    private ObjectInputStream in = null;
    private ObjectOutputStream out = null;
    private String currentClient;
    private volatile boolean isConnected;

    public ServerThread(Socket socket, DataProvider dataProvider) {
        this.isConnected = true;
        this.dataProvider = dataProvider;
        this.socket = socket;
        try {
            this.in = new ObjectInputStream(socket.getInputStream());
            this.out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            while (isConnected) {
                String receivedMessage = (String) this.in.readObject();
                System.out.println("Received: " + receivedMessage);
                handleRequest(receivedMessage);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            socket.close();
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void handleRequest(String message) {
        /*This method handle all requests from clients.*/

        String messagePack = null;

        if (message.startsWith("conn")) {
            messagePack = connect(message);
        }
        if (message.startsWith("create")) {
            messagePack = addNewUser(message);
        }
        if (message.equals("1")) {
            messagePack = dataProvider.getAllTopics();
        }
        if (message.startsWith("2")) {
            messagePack = subscribe(message);
        }
        if (message.equals("3")) {
            messagePack = dataProvider.getClientTopics(currentClient);
        }
        if (message.startsWith("4")) {
            messagePack = dataProvider.getNewsAboutATopic(currentClient, StringConverter.extractMessage("4", message));
        }
        if (message.equals("0")) {
            messagePack = "You are disconnected now.";
            dataProvider.getLoggedClients().remove(currentClient);
            isConnected = false;
        }

        try {
            this.out.writeObject(messagePack);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String connect(String message) {
        /*In this method we well try to handle a connect request.
        We check if the username exists and if the client is already logged in.
        If everything is fine we are connecting the client to the server and set the current client.*/

        String username = StringConverter.extractMessage("conn", message);
        Map<String, List<Topic>> clients = dataProvider.getClients();
        if (dataProvider.getLoggedClients().contains(username)) {
            return "You are already logged in.";
        } else {
            if (clients.containsKey(username)) {
                dataProvider.getLoggedClients().add(username);
                currentClient = username;
                return "You are connected now.";
            } else
                return "This username does not exists. Please try again!";
        }
    }

    private String addNewUser(String message) {
        /*Here we are adding a new client to our server.
        If the username is already taken, we send a message to the client to try again.
        If the username is unique, we connect the client to server and add him to our clients.*/

        String newUsername = StringConverter.extractMessage("create", message);
        Map<String, List<Topic>> clients = dataProvider.getClients();
        if (clients.containsKey(newUsername)) {
            return "This username is taken! Please, try again...";
        }
        clients.put(newUsername, new ArrayList<>());
        dataProvider.getLoggedClients().add(newUsername);
        currentClient = newUsername;

        return "You are registered and connected now.";
    }

    private String subscribe(String message) {
        String topic = StringConverter.extractMessage("2", message);
        Map<String, List<Topic>> clients = dataProvider.getClients();

        if (dataProvider.containsTopic(topic)) {
            if (!dataProvider.clientSubscribeTopic(currentClient, topic)) {
                clients.get(currentClient).add(dataProvider.getTopicByName(topic));
                return "You are subscribed now to " + topic + " topic.";
            } else {
                return "You are already subscribed to this topic.";
            }
        } else {
            return "This topic does not exists.";
        }
    }
}
